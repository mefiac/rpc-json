<?php

use yii\db\Schema;
use yii\db\Migration;


class m200402_235201_data_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->createTable('data', [
            'id' => Schema::TYPE_PK,
            'page_uid' => Schema::TYPE_STRING . ' NOT NULL',
            'content' => Schema::TYPE_TEXT,
            'author' => Schema::TYPE_TEXT,
            'created' => Schema::TYPE_DATETIME. ' NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('data');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200402_235201_data_table cannot be reverted.\n";

        return false;
    }
    */
}
