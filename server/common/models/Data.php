<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "data".
 *
 * @property int $id
 * @property string $page_uid
 * @property string|null $content
 * @property string|null $created
 */
class Data extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['page_uid'], 'required'],
            [['content'], 'string'],
            [['created'], 'safe'],
            [['page_uid'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_uid' => 'Page Uid',
            'content' => 'Content',
            'created' => 'Created',
        ];
    }

    public function updateDataToModel($params)
    {
        $this->page_uid = $params->page_uid;
        $this->content = $params->content;
    }
}
