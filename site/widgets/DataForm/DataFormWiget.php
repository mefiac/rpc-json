<?php

namespace app\widgets\DataForm;

use app\components\RpcClient;
use app\models\DataForm;
use yii\base\Widget;


class DataFormWiget extends Widget
{
    /**
     * @var
     */
    public $page_uid = null;
    public $action = 'get-data';

    public function init()
    {
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        $model = new DataForm();
        if ($this->page_uid) {
            $client = new RpcClient('http://rpc.my/');
            $client->method = $this->action;
            $client->auth = \Yii::$app->session["_userInfo"];
            $data = $client->sendData(["page_uid" => $this->page_uid]);
            if (!isset($data->result->message) && $data->result->message != 'error')

                $model->setFormData($data->result[0]);

        }

        return $this->render('index', ["model" => $model]);
    }

}