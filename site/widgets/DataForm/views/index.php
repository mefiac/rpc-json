<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$form = ActiveForm::begin([
    'id' => 'data-form',
    'layout' => 'horizontal',
    'fieldConfig' => [
        'labelOptions' => ['class' => 'col-lg-1 control-label'],
    ],
]); ?>

<?= $form->field($model, 'page_uid')->textInput(['aria-required' => true, 'aria-invalid' => false]) ?>
<?= $form->field($model, 'content')->textInput() ?>
<?= $form->field($model, 'author')->textInput() ?>
<?= $form->field($model, 'created')->textInput() ?>
<div class="form-group">
    <div class="col-lg-offset-1 col-lg-11">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
    </div>
</div>

<?php ActiveForm::end(); ?>
