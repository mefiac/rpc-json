<?php

namespace app\models;

use yii\base\Model;

class DataForm extends model
{

    public $page_uid;
    public $content;
    public $author;
    public $created;

    /**
     * @param $data
     */
    public function setFormData($data)
    {
        $this->page_uid = $data->page_uid;
        $this->content = $data->content;
        $this->author = $data->author;
        $this->created = $data->created;
    }


}