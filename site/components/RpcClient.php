<?php

namespace app\components;

use yii\httpclient\Client;

class RpcClient
{
    private $client;
    public $id = 2;
    public $method;
    public $auth = null;
    public $params;
    const TYPE_STRING = ["jsonrpc" => "2.0"];

    public function __construct($url)
    {
        $this->client = new Client(['baseUrl' => $url]);
    }

    public function sendData(array $params)
    {
        $this->id = rand(0, 10000);
        $params_json = "[" . json_encode($params) . "]";
        $body = json_encode(array_merge($this::TYPE_STRING, ["id" => $this->id], ["method" => $this->method], ["auth" => $this->auth], ["params" => "%replase%"]), JSON_FORCE_OBJECT);
        $body = str_replace('"%replase%"', $params_json, $body);
        /** @var Client $response */
        $response = $this->client->createRequest()
            ->addHeaders(['content-type' => 'application/json'])
            ->setContent($body)
            ->send()->content;
        $answer = json_decode($response);

        return $answer->id != $this->id ? false : $answer;
    }

}